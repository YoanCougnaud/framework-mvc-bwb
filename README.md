# FRAMEWORK PHP MVC BEWEB 
## Description
Ce projet est une etude de php a travers la mise en place d'un "framework" basé sur le modèle mvc   
Cette implémentation est le résultat du TD mis en place au sein de l'ecole beWeb.   

## Installation
1. clonez le dépôt a la racine de votre serveur web.
2. mettez en place les virtualhosts (si necessaire).
3. installez les dépendances avec la commande : composer install
4. configurez les accés en base de données dans le fichier database.json
5. effetuez une requette http a la racine (ex : http://php-mvc.bwb/)
6. Vous devrez voir apparaitre le texte suivant : FRAMEWORK MVC PHP beWeb

## Structure
Le framework s'appuie sur le modèle MVC afin de s'assurer la séparation des tâches.   
Il est donc livré avec les dossiers et les fichiers permettant de pouvoir travailler sur la logique métier.   

1. Le dossier core/   
il contient tous les fichiers necessaires au fonctionnement du framework. 
2. Le dossier config/   
Contient les fichiers de configuration :
 * database.json : configuration de la base de données
 * routing.json : configuration du mapping entre les routes et les controleurs
3. Le dossier controllers/   
Doit contenir les controleurs qui vont être invoqués lors des requêtes http
4. Le dossier dao/   
Doit contenir les fichiers DAO pour l'accés aux données
5. Le dossier models/   
Doit contenir les entités correspondant a la logique metier de l'application
6. Le dossier views/   
Doit contenir toutes les vues de l'application
7. Le dossier vendor/   
Qui contient les dépendances récupérées via composer
8. A la racine   
 * le fichier index.php qui est le point d'entrée de l'application
 * le fichier composer.json qui est le fichier de configuration de composer
 * le fichier .htaccess qui override la configuration d'apache (assurez vous d'avoir le mod_rewrite activé et opérationnel)

## Fonctionnement

### Prise en main

### Controller

### DAO

### views

### Security middleware
 
### Plugin

## Description

Le plugin est composé de 3 fichier ayant pour but de créer les classes dédiées aux entitées, le plugin va donc créer deux fichier PHP class un pour l'entité elle même ainsi qu'un autre pour le DAO prévu pour cette même entité. Le dernier fichier regroupe les deux premiers afin de pouvoir automatiser la création intégrale des models ainsi que des DAO dédiés

## Préparation

Assurez vous d'avoir compléter les étapes prévues dans le cadre de l'installation du framework avant toute chose.

## Utilisation

Une fois l'installation complétée avec succès vous pouvez désormais ouvrir un terminal à la racine de votre projet afin de lancer le fichier config.php.
Ce fichier permet de créer vos fichiers de 3 manières différentes. Dans votre terminal avec la ligne de commande :

 * php config.php generate all : c'est la commande qui est entièrement automatiser  elle requiert uniquement que vous ayez spécifié le nom de votre base de données dans le fichier database.json, elle va vous créer toutes les entitées présentes dans votre base de données et les DAO associées.

 * php config.php class nomDeVotreTable : cette commande va créer uniquement la classe de l'entité que vous avez passé après le mot "class", attention à bien écrire le nom exactement comme dans votre base de données.

 * php config.php dao nomDeLaClasse : cette fois ci cette commande va rechercher dans votre projet le fichier de la classe que vous avez renseigné dans la commande.

 * php config.php nomDeLaTable : cette commande va créer la classe de l'entité ainsi que le DAO en fonction du nom de la table que vous avez passé en argument.

## Disclaimer

Le plugin ne prend pas en charge les clés étrangères faites donc attention, le plugin vous facilite la vie du point de vue de la création de vos fichiers mais vous devrez les vérifier et les modifier afin qu'ils puissent coller à vos besoins. 